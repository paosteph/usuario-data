const crear = require('./crear');
const buscarUno = require('./buscar');
const buscarCrear = require('./buscar-crear');
const eliminar = require('./eliminar');
const buscarTodos = require('./buscar-todos');
const datosPrueba = require('../datos-prueba/datos-prueba')

module.exports = {
    crear,
    buscarUno,
    buscarCrear,
    eliminar,
    buscarTodos,
    datosPrueba
}