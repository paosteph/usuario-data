module.exports = (patronBusqueda, arregloUsuarios, callback) => {
    const coincidencias = arregloUsuarios.filter((usuario) => {
        return usuario.nombre.search(patronBusqueda) !== -1;
    });

    const existenCoincidencias = coincidencias.length > 0;
    if(existenCoincidencias){
        callback({
            mensaje: 'Existen coincidencias',
            usuariosEncontrados: coincidencias
        });
    }else{
        callback({
            mensaje: 'No se encontro alguna coincidencia',
        });
    }
}