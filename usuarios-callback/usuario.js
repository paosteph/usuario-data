const usuarioLib = require('./libreria-usuario');

const registrarUsuario = (usuario, arreglo) => {
    usuarioLib.buscarCrear(usuario, arreglo, (respuesta) => {
        console.log(respuesta)
    });
}

registrarUsuario(
    usuarioLib.datosPrueba.usuarioBusquedaValido,
    usuarioLib.datosPrueba.arregloUsuarios
)

const eliminarUsuario = (usuario, arreglo) => {
    usuarioLib.eliminar(usuario, arreglo, (respuestaEliminar) => {
        console.log(respuestaEliminar);
    })
}

// eliminarUsuario(
//     {
//         nombre: 'Pao'
//     },
//     arregloUsuarios
// );

const buscarTodos = (patronBusqueda, arreglo) => {
    usuarioLib.buscarTodos(patronBusqueda, arreglo, (resultadoTodos) => {
        console.log(resultadoTodos);
    });
}

buscarTodos(
    usuarioLib.datosPrueba.patronBusquedaValido,
    usuarioLib.datosPrueba.arregloUsuarios
);

