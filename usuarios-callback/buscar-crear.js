const buscar = require('./buscar');
const crear = require('./crear');

module.exports = (usuario, arreglo, callback) => {
    buscar(usuario, arreglo, (respuestaBusqueda)=>{
        if(respuestaBusqueda.mensaje === 'Usuario no encontrado'){
            crear(usuario, arreglo, (respuestaCreacion) => {
                callback({
                    mensaje: 'Usuario no se encontro, pero ya se creo',
                    arreglo: respuestaCreacion.arreglo
                });
            })
        }else{
            callback(respuestaBusqueda);
        }
    })
}