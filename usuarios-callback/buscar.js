module.exports = (usuario, arreglo, callback) => {
    var indiceBuscado = -1;
    const buscado = arreglo.find((usuarioActual, indice) => {
        const existeUsuario = usuario.nombre === usuarioActual.nombre;
        if(existeUsuario){
            indiceBuscado = indice;
            return true;
        }
        return false;
    });

    if(buscado){
        callback({
            mensaje: 'Usuario encontrado',
            indiceBuscado,
            usuario: buscado
        });
    }else{
        callback({
            mensaje: 'Usuario no encontrado',
            indiceBuscado
        });
    }
}