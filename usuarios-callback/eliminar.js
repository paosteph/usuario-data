const buscar = require('./buscar');

module.exports = (usuario, arreglo, callback) => {
    var indiceUsuario;
    buscar(usuario, arreglo, (respuestaBusqueda) => {
        indiceUsuario = respuestaBusqueda.indiceBuscado;
    });
    const existeUsuario = indiceUsuario > -1
    if (existeUsuario){
        const eliminado = arreglo.splice(indiceUsuario,1);
        callback({
            mensaje: 'Usuario eliminado',
            usuario: eliminado
        })
    }else{
        callback({
            mensaje: 'Usuario no eliminado ya que no existe en lista actual',
        })
    }
}