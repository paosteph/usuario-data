module.exports.arregloUsuarios = [
    {
        nombre: 'Camila-san'
    },
    {
        nombre: 'Beto-san'
    },
    {
        nombre: 'Juanito-san'
    }
]

module.exports.parametrosUsuarios = [
    {
        nombre: 'Pao'
    },
    this.arregloUsuarios
];

module.exports.patronBusquedaValido = 'san';
module.exports.patronBusquedaNoValido = ' ';

module.exports.parametrosBusquedaTodos = [
    this.patronBusquedaValido, 
    this.arregloUsuarios
];

module.exports.parametrosBusquedaTodosNoValidos = [
    this.patronBusquedaNoValido, 
    this.arregloUsuarios
];

module.exports.usuarioBusquedaValido = {
    nombre: 'Pao'
}

module.exports.usuarioBusquedaNoValido = {
    nombre: 12345
}
