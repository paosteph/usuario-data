const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

const buscarCrear = require('../usuarios-promesas/buscar-crear');


module.exports = (parametros) => {
    const buscarCrear$ = from(buscarCrear(parametros[0], parametros[1]));
    return buscarCrear$;
}