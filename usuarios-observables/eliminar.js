const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');
const eliminar = require('../usuarios-promesas/eliminar')

module.exports = (parametros) => {
    const eliminar$ = from(eliminar(parametros[0], parametros[1]));
    return eliminar$;
}