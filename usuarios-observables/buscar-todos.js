const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');
const buscarTodos = require('../usuarios-promesas/buscar-todos')

module.exports = (parametros) => {
    const buscarTodos$ = from(buscarTodos(parametros[0], parametros[1]));
    return buscarTodos$;
}