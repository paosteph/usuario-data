const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

const promesaBuscar = require('../usuarios-promesas/buscar.js');

module.exports = (parametros) => {
    
    const buscar$ = from(promesaBuscar(parametros[0], parametros[1]));
    return buscar$;
}