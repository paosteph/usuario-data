const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

const promesaCrear = require('../usuarios-promesas/crear');

module.exports = (parametros) => {
    
    const crear$ = from(promesaCrear(parametros[0],parametros[1]));
    return crear$;
}
