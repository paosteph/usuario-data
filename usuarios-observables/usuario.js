const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

const usuarioLibreria = require('./libreria-usuario')


const respuestaOK = (respuesta) => {
    console.log(respuesta);
};
const error = (error) => {
    console.log(error);
}


const usuarios$ = of(usuarioLibreria.datosPrueba.parametrosUsuarios);
usuarios$
    .pipe(
        mergeMap(usuarioLibreria.crear),
        
    )
    .subscribe(
        respuestaOK,
        error
    )


