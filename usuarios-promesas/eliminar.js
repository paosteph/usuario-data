const buscar = require('./buscar');

module.exports = (usuario, arregloUsuarios) => {
    let indiceUsuario;
    
    const resultadoBusquedaUsuario = (respuestaBusqueda) => {
        indiceUsuario = respuestaBusqueda.indiceBuscado;
    }
    const errorBusquedaUsuario = (resultadoNoCoincidencias) => {
        indiceUsuario = -1;
    }
    buscar(usuario, arregloUsuarios)
    .then(resultadoBusquedaUsuario)
    .catch(errorBusquedaUsuario)

    const resultadoPromesaELiminarUsuario = (resolve, reject) => {
        const existeUsuario = indiceUsuario > -1;
        if (existeUsuario){
            const eliminado = arreglo.splice(indiceUsuario,1);
            resolve({
                mensaje: 'Usuario eliminado',
                usuario: eliminado
            });
        }else{
            reject({
                mensaje: 'Error eliminando, usuario no existe',
            });
        }
    }

    return new Promise(resultadoPromesaELiminarUsuario);
    
}