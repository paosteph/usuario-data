const crear = require('./crear');
const buscar = require('./buscar');
const buscarCrear = require('./buscar-crear');
const eliminar = require('./eliminar');
const buscarTodos = require('./buscar-todos');
const datosPrueba = require('../datos-prueba/datos-prueba');

module.exports = {
    crear,
    buscar,
    buscarCrear,
    eliminar,
    buscarTodos,
    datosPrueba
}