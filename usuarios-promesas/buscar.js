module.exports = (usuario, arreglo) => {
    var indiceBuscado = -1;
    const usuarioBuscado = arreglo.find((usuarioActual, indice) => {
        const existeUsuario = JSON.stringify(usuario) === JSON.stringify(usuarioActual);
        if(existeUsuario){
            indiceBuscado = indice;
            return true;
        }
        return false;
    });

    const respuestaPromesaBuscar = (resolve, reject) => {
        if(usuarioBuscado){
            resolve({
                mensaje: 'Usuario encontrado',
                indiceBuscado,
                usuario: usuarioBuscado
            });
        }else{
            reject({
                mensaje: 'Error: Usuario no encontrado',
                indiceBuscado
            });
        }
    }
    
    return new Promise(respuestaPromesaBuscar);
    
}