const buscar = require('./buscar');
const crear = require('./crear');

module.exports = (usuario, arreglo) => {

    const resultadoBusquedaUsuario = (respuestaEncontro) => {
        resolve(respuestaEncontro);
    }

    const errorBusquedaUsuario = (respuestaNoEncontro) => {
        return crear(usuario, arreglo)
    }

    const resultadoCreacionUsuario = (respuestaCreacion) => {
        resolve({
            mensaje: 'Usuario no se encontro, pero ya se creo',
            arreglo: respuestaCreacion.arreglo
        });
    }
    
    const respuestaPromesaBuscarCrearUsuario = (resolve, reject) => {
        buscar(usuario, arreglo)
        .then((respuestaEncontro) => {
            resolve(respuestaEncontro);
        })
        .catch(errorBusquedaUsuario)
        .then((respuestaCreacion) => {
            resolve({
                mensaje: 'Usuario no se encontro, pero ya se creo',
                arreglo: respuestaCreacion.arreglo
            });
        });
        
    }
    return new Promise(respuestaPromesaBuscarCrearUsuario);   
    
}