module.exports = (patronBusqueda, arregloUsuarios) => {
    const coincidenciasUsuarios = arregloUsuarios.filter((usuario) => {
        return usuario.nombre.search(patronBusqueda) !== -1;
    });

    const respuestaPromesaBuscarTodosUsuarios = (resolve) => {
        const existenCoincidencias = coincidenciasUsuarios.length > 0;
        if(existenCoincidencias){
            resolve({
                mensaje: 'Existen coincidencias',
                usuariosEncontrados: coincidenciasUsuarios
            });
        }else{
            resolve({
                mensaje: 'No se encontro alguna coincidencia',
            });
        }
        }

    return new Promise(respuestaPromesaBuscarTodosUsuarios);
    
}