const usuarioLibreria = require('./libreria-usuario')

const registrarUsuario = (usuario, arreglo) => {
    usuarioLibreria.buscarCrear(usuario, arreglo)
    .then((respuesta) => {
        console.log(respuesta);
    })
}

registrarUsuario(
    usuarioLibreria.datosPrueba.usuarioBusquedaNoValido,
    usuarioLibreria.datosPrueba.arregloUsuarios
);

const buscarTodos = (patronBusqueda, arreglo) => {
    usuarioLibreria.buscarTodos(patronBusqueda, arreglo)
    .then((respuestaTodos) => {
        console.log(respuestaTodos)
    })
    .catch((respuestaNoCoincidencias) => {
        console.log(respuestaNoCoincidencias)
    })
    
}

buscarTodos(
    usuarioLibreria.datosPrueba.patronBusquedaValido,
    usuarioLibreria.datosPrueba.arregloUsuarios
);

const eliminarUno = (usuario, arreglo) => {
    usuarioLibreria.eliminar(usuario, arreglo)
    .then((respuestaEliminacion) => {
        console.log(respuestaEliminacion)
    })
    .catch((falloEliminacion) => {
        console.log(falloEliminacion)
    });
};

eliminarUno(
    usuarioLibreria.datosPrueba.usuarioBusquedaValido,
    usuarioLibreria.datosPrueba.arregloUsuarios
)